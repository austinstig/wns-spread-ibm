# ReadMe

WNS-IBM is an Individual Based Model that is used to model the spread of White-Nose Syndrome through bat populations. It leverages geoexpression to exploit geographic process concurrency. It is developed using Rust and the SPECS Parallel ECS library.

## Installation

Running the install.sh file within the scripts folder will install git, rustup, clone the repository, and compile the code locally.

## Compilation
The Rust compiler is used to compile the model. Simply invoke the rust commands `cargo build --release` in the project directory.

## Execution

To execute an example run of the model change to the directory where the model repository was downloaded and invoke the following command from the command prompt
$ ./target/release/wns-model <repo-directory>/examples/test/config.toml


## Configuration
The model uses a series of configuration files to construct the landscape comprising of roost sites, the initial bat population, and the species parameters of the bats included in the initial bat population.

The overarching configuration file passed into the model is given in Tom's Obvious Markup Language (TOML) format. The parameters of the file are defined below:

1. dispatch - Either `true` or `false`. If `true` the model will distribute the computation over multiple processors. Otherwise the model will not run in parallel mode.
2. max_pop - The maximum bat population, expressed as a whole number, that can be allowed within the model. This is used as a mechanism to constrain the population dynamics of the bat populations in the context of real-world resource constraints (because the Earth cannot support and infinite bat population).
3. years - The numbe of model years, expressed as a whole number, to simulate.
4. seed - The seed value, expressed as a whole number, between 0 and 2^64 to seed the random number generator. Running the model with the same inputs and seed value will produce the same outcomes.
5. roost_csv - The file path to the roost site data.
6. bats_csv - The file path to the initial bat population data.
7. species_csv - The file path to the species data.
8. output_directory - The directory to output the model's results to.
9. thread_count - The number of CPU threads to instantiate to run the model in dispatch mode.
10. human_pop - The number of humans to represent in the model. *If not using Bat Only Transmission*


### Roost Sites
The roost configuration is specified in a roost configuration file: roost.csv. It should contain every roost instantiated into the model for a given run. The roost site parameters are as follows:
1. lat - The latitude of the roost site, expressed in decimal degrees, used for computing the haversine distance between it and every other roost site.
2. lon - The Longitude of the roost site, expressed in decimal degrees, used for computing the haversine distance between it and every other roost site.
3. fips - The Federal Information Processing Standards (FIPS) code for the county where the roost site is located. This is used to provided county-level aggregation in the model's output.
4. max_pop - The maximum bat population, expressed as a whole number, that the roost site can sustatin.
5. init_infection_state - The initial infection state of the roost site. The values can either be `true` or `false`.
6. mean_temp - The mean annual temperature expressed as a decimal in degrees C of the roost site. Used as a constraint for the growth of the pathogen at the roost site.

### Initial Bat Population
The initial bat population configuration is specified in a bat configuration file: bats.csv. It should contain every bat initially instantiated into the model for a given run. The bat parameters are as follows:
1. species - The species of the bat expressed as a species key value as listed in the species.csv file.
2. gender - The gender of the bat expressed as either `male` or `female`. Used to inform fecundity and reproduction of the population.
3. age - The age of the bat expressed as a whole number.
4. init_infection_state - The initial infection state of the bat. The values can either be `true` or `false`.
5. roost - The number of the roost site it initially occurs in. The number is based on the line number of the roost site as listed in the roost.csv file.


### Species
The species configuration is specified in a species configuration file: species.csv. It useses a series of commonly known species keys to identify the species of bat. The keys are listed here with their common names:

1. CORA - Rafineque's Big-eared Bat
2. EPFU - Big Brown Bat
3. LANO - Silver-haired Bat
4. LABO - Eastern Red Bat
5. LACI - Hoary Bat
6. LAIN - Northern Yellow Bat
7. LASE - Seminole Bat
8. MYAU - Southeastern Myotis
9. MYGR - Grey Bat
10. MYSE - Norther Myotis
11. MYLU - Little Brown Bat
12. MYSO - Indiana Bat
13. NYHU - Evening Bat
14. PISU - Eastern pipistrelle

For each species defined a series of parameters must also be defined:
1. max_age - The maximum age that the species can live expressed in terms of the number of years.
2. fecundity - The fecundity of the species expressed as a probability from 0 to 1 where 0 is no fecundity and 1 is 100% reproductive.
3. migration_distance - The distance in kilometers that a bat can travel radially from a given roost site during swarm.
4. infection_rate - The probability a bat will become infected expressed from 0 to 1 where 0 is no chance of becoming infected and 1 is 100% chance. This applies when the bat comes into contact with the pathogen.
5. fidelity - The fidelity of the bat to its winter roost site expressed as a probability from 0 to 1 where 0 is expressing no fidelity and 1 is 100% fidelity.
6. visitation_rate - The visitation rate of a bat to the number of roost sites within its migration distance during Swarm. Expressed from 0 to 1 where 0 means the bat will not visit any other roosts and 1 means the bat will visit 100% of the other roosts.
7. disease_mortality_rate - The probabililty that a bat will die after becoming infected by the disease expressed in terms from 0 to 1. 0 means it will not die as a result of the infection and 1 means it will definitely die.

## Scripts
The scripts folder contains a set of scripts used for various purposes.

### Installation Helper
An installer script is provided for Ubuntu systems. It installs git, rust, and the model.

### Roost Site Generation
The roost sites were generated using the methodology laid out in the `generate-roost-sites-from-shp.R` script. The script accepts a shapefile containing all of the generated roost sites. By providing the FIPS code to intitially infect, the script will generate a roost dataset as output.

### Bat Population Generation
The initial bat population is generated using the `entity-spawner.R` script. Once the roost.csv file has been created by the `generate-roost-sites-from-shp.R` script, an initial bat population can be generated. The roost.csv is passed as an argument to the `entity-spawner.R` script, as well as the number of bats to generate for the initial population. The script is simplistic in that it does not provide a mechanism to initially infect any bats automatically. Manual edits to the output file enable the user to set the initial infection states for specific bats to `true`. Also, the script only supports the generation of an initial bat population as a single species "MYLU". This can also be manually changed by the user because the model can support many species.

### Plot Generation
Not specifically needed to run the model, the `compute-plots.R` script can be used to generate plots depicting the relationship between each System's execution time and the problem size provided.

## Examples
The examples folder contains a set of example runs of the model.

### Test
The test example tests that the model runs with a small input dataset.

### Human Summer
The human-summer example runs the model with human transmission during the summer. To run this example be sure to compile the code base using the `human-summer` branch.

### Human Winter
The human-winter example runs the model with human transmission during the winter. To run this example be sure to compile the code base using the `human-winter` branch.

### Bat Only
The bat-only example runs the model without any human transmission.
