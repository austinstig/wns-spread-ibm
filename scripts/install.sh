
# check for the installation of Git
if ! [ -x "$(command -v git)" ]; then
  echo "installing git...";
  sudo apt update
  sudo apt install git
fi

# check for the installation of rustup
if ! [ -x "$(command -v rustup)" ]; then
    echo "installing rustup, an installer for Rust...";
    curl https://sh.rustup.rs -sSf | sh
fi

# download the WNS model using git
git clone https://bitbucket.org/austinstig/wns-spread-ibm.git

# build the WNS model
cd ./wns-ibm
cargo build --release
