//! # Build-Landscape
//! A module with a function for generating the
//! landscape from a list of roost sites.
use super::*;

/// construct a graph with distances as edge weights
/// construct a landscape based on a network of neighboring roost sites. Roost Sites
/// are adjacent if the distance between them is within the radius parameter.
pub fn build_landscape(mut roosts: Vec<RoostSite>, radius: Length) -> Landscape {


        // create a new graph structure
        let mut graph = helpers::make_landscape(&mut roosts);

        // clone the sites for faster distance calculation
        let mut sites: Vec<RoostSite> = roosts
            .iter()
            .cloned()
            .collect();

        debug!("radius: {:#?}m",radius);

        // compute visitation likelihood denominator
        let vr_total: f32 = sites
            .iter()
            .fold(0.0, |vr, site| vr + site.visitation_multiplier);
        
        // update visitation multiplier for global liklihood
        sites.iter_mut()
             .for_each(|mut site| site.visitation_multiplier /= vr_total);

        // generate all point pairs for the roost sites
        sites.iter()
             .enumerate()
             .for_each(|(idx, node_a)| {

                // compute distances from other
                // roost sites to this node. Add
                // an edge between the two roosts
                // if it is <= the radius.
                sites.iter()
                     .skip(idx+1)
                     .for_each(|node_b| {

                        // compute distances between nodes
                        let distance = node_a.distance(&node_b);
                        debug!("distance: {0:#?} <-> {1:#?} = {2:#?}m", node_a, node_b, distance);

                        // compute distance and set edge if <= radius
                        if let (Some(a), Some(b)) = (node_a.id.index(), node_b.id.index()) {
                            if distance <= radius { graph.add_edge(a,b,distance); }
                        }
                });
        });
    
        // return graph
        graph
}

/// resolve roost sites locations based on landscape
pub fn resolve_roost_sites(roosts: &Vec<RoostSite>, bats: &mut Vec<Bat>) {

    // generate a temporary list of roost ids
    let mut temps: Vec<RoostId> = roosts.into_iter().map(|roost| roost.id).collect();
    
    // loop over all the bats and update
    // the roost id the bat resides in with
    // the id of the roost taken from the
    // landscape.
    bats.iter_mut()
        .enumerate()
        .for_each(|(_,bat)| {
            // set the index to remove
            let mut removal: usize = 0;
            let mut trigger: bool = false;

            // scan the set of roost ids for
            // the bat roost location.
            for (idx, roost) in temps.iter().enumerate() {
                if bat.roost.base() == roost.value() {

                    // determine removal index
                    removal = idx;
                    trigger = true;

                    // update the roost id value
                    if let Some(nid) = roost.index() {
                        bat.roost.set_node_index(nid);
                    }

                    // break the loop
                    break;
                }
            }

            // remove the removal index from the set of temps
            if trigger { temps.remove(removal); }
    });

}

/// Add landscape and roosts to the ECS world
pub fn add_landscape_to_world(world: &mut World, landscape: Landscape) {
    // add the landscape as a resource
    world.add_resource(landscape);
}

/// Add a clock to manage time in the ECS world
pub fn add_clock_to_world(world: &mut World) {
    // construct a new clock
    let clock = Clock::new();
    // add the landscape as a resource
    world.add_resource(clock);
}

/// Add bats to the ECS world
pub fn add_bats_to_world(world: &mut World, mut bats: Vec<Bat>) {
    // add bats to the world and update
    // their roost identities.
    bats.iter_mut().for_each(|bat| {
        // update bat roost id to include an index
        bat.roost.promote();
        // create a world entity
        world.create_entity()
            .with(bat.species)
            .with(bat.gender)
            .with(bat.infected)
            .with(bat.lifecycle)
            .with(bat.roost)
            .with(RandomMovement1::default())
            .with(RandomMovement2::default())
            .with(RandomMovement3::default())
            .with(RandomMovement4::default())
            .with(RandomReproduction1::default())
            .with(RandomReproduction2::default())
            .with(RandomReproduction3::default())
            .with(RandomDiseaseMortality::default())
            .with(RandomAgeMortality::default())
            .build();
    });
}

/// setup a resource that stores the human movement rate
pub fn add_human_movement_to_world(world: &mut World, conf: &Configuration) {
    // human movement resource
    let hmovement = HumanMovement::new(conf.human_pop);
    // add to world
    world.add_resource(hmovement);
}

/// setup a total population size resource
pub fn add_population_size_to_world(world: &mut World) {
    // population resource
    let pop = Population::default();
    // add to world
    world.add_resource(pop);
}

/// Add species to the ECS world as a resource
pub fn add_species_to_world(world: &mut World, species: Vec<Species>) {

    // construct a hashmap of species
    let mut map: SpeciesTable = SpeciesTable::new();

    // convert species vector to hashmap
    for sp in species.into_iter() {
        map.insert(sp.name.clone(), sp);
    }

    // add hashmap to world as resource
    world.add_resource(map);
}

/// initialize a world given configuration files.
pub fn init(settings: &Configuration) -> Result<World, WNSError> {

    // load the roost site information
    let roosts: Vec<RoostSite> = load_roost_sites(&settings.roost_csv)?;

    // load the bats of the world
    let mut bats = load_bats(&settings.bats_csv)?;

    // load the species of the world
    let species = load_species(&settings.species_csv)?;

    // determine the radius as the maximum
    // migration distance provided in the
    // species file.
    let radius: Length = species.iter()
        .cloned()
        .map(|sp| sp.migration_distance)
        .fold(Length::new::<kilometer>(0f32), Length::max);
    
    println!("radius is: {:#?}", radius);

    // construct the landscape by using the radius to link
    // roost sites on the landscape.
    let landscape = build_landscape(roosts.clone(), radius);

    // resolve the roost site identities
    resolve_roost_sites(&roosts, &mut bats);

    // construct a world for the ECS
    let mut world = World::new();

    // add world components for bats
    world.register::<SpeciesKey>();
    world.register::<Gender>();
    world.register::<Infected>();
    world.register::<Lifecycle>();
    world.register::<RoostId>();
    world.register::<RandomMovement1>();
    world.register::<RandomMovement2>();
    world.register::<RandomMovement3>();
    world.register::<RandomMovement4>();
    world.register::<RandomReproduction1>();
    world.register::<RandomReproduction2>();
    world.register::<RandomReproduction3>();
    world.register::<RandomDiseaseMortality>();
    world.register::<RandomAgeMortality>();

    // add a clock to the world
    add_clock_to_world(&mut world);

    // add human movement to world
    add_human_movement_to_world(&mut world, &settings);

    // add landscape to world
    add_landscape_to_world(&mut world, landscape);

    // add the species to the world as ECS resource
    add_species_to_world(&mut world, species);

    // add the initial bats as entities of the world
    add_bats_to_world(&mut world, bats);

    // add population to world
    add_population_size_to_world(&mut world);

    // return world
    Ok(world)
}
