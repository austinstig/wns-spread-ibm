//! # Build-Landscape
//! A module with a function for generating the
//! landscape from a list of roost sites.
use super::*;

/// add a roost site to a landscape. It also updates the roost
/// id value to reflect that of the landscape.
pub fn add_roost(landscape: &mut Landscape, roost: &mut RoostSite) {

    // get the node id value for the 
    // roost site added to the graph
    let nid = landscape.add_node(roost.clone());

    // update the roost id of the roost
    roost.id.set_node_index(nid);

    // update th enode weight for the graph
    if let Some(n) = landscape.node_weight_mut(nid) {
        n.id.set_node_index(nid);
    }
}

/// construct the landscape graph
pub fn make_landscape(roosts: &mut Vec<RoostSite>) -> Landscape {

    // create a new graph structure
    let mut graph = Landscape::new_undirected();

    // assign the roost sites id values based on the
    // NodeIndex values produced when adding them to
    // the landscape.

    for (_, mut roost) in roosts.iter_mut().enumerate() {

        // add the roost
        add_roost(&mut graph, &mut roost);
    }

    // return the graph landscape
    graph
}

/// The world resources are the tokens of data in the model
pub fn tokens(world: &mut World) -> &mut Resources {
    &mut world.res
}

/// The world resources are the tokens of data in the model
pub fn tokens_ref(world: &World) -> &Resources {
    &world.res
}