use super::*;

/// # Temperature
/// The mean temperature at a roost site.
#[derive(Component,Deserialize, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Temperature {
    /// maximum population the roost can sustain
    pub mean_temp: f32,
}

impl Temperature {
    pub fn ok_for_pathogen(&self) -> bool {
        self.mean_temp <= 20. && self.mean_temp >= 5.
    }
}