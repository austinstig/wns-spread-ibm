//! # Types
//! Useful types for the system
use super::*;

// load modules
mod species;
mod roostsite;
mod bat;
mod clock;
mod capacity;
mod location;
mod temperature;
mod infected;
mod identity;
mod gender;
mod lifecycle;
mod population;
mod random;
mod human;
mod intensity_tracking;

// re-export the types
pub use crate::types::species::*;
pub use crate::types::roostsite::*;
pub use crate::types::bat::*;
pub use crate::types::clock::*;
pub use crate::types::capacity::*;
pub use crate::types::location::*;
pub use crate::types::temperature::*;
pub use crate::types::infected::*;
pub use crate::types::identity::*;
pub use crate::types::gender::*;
pub use crate::types::lifecycle::*;
pub use crate::types::population::*;
pub use crate::types::human::*;
pub use crate::types::random::*;
pub use crate::types::intensity_tracking::*;

// type aliases
pub type SpeciesTable = HashMap<SpeciesKey, Species>;