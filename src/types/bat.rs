//! # Bat
//! A sub-module that defines a bat.
use super::*;

/// # Bat
/// Bats are the primary agents of the model.
/// Their attributes are defined in a configuration
/// csv file.
#[derive(Component,Deserialize, Debug, Clone)]
#[storage(VecStorage)]
pub struct Bat {

    // the species of bat
    #[serde(deserialize_with="deserialize_species_key")]
    pub species: SpeciesKey,

    // the gender of the bat
    #[serde(deserialize_with="deserialize_gender")]
    pub gender: Gender,

    // the age condition of the bat
    #[serde(default)]
    #[serde(rename="age")]
    #[serde(deserialize_with="deserialize_lifecycle")]
    pub lifecycle: Lifecycle,

    // current infection state of bat
    #[serde(rename="init_infection_state")]
    #[serde(deserialize_with="deserialize_infected")]
    pub infected: Infected,

    // initial roost location
    #[serde(flatten)]
    #[serde(default)]
    #[serde(deserialize_with="deserialize_roost_id")]
    pub roost: RoostId
}

impl Bat {

    /// birth -- copy mothers attributes and assign a roost location
    pub fn birth(species: SpeciesKey, gender: Gender, roost: &RoostId) -> Bat {
        Bat {
            species: species,
            gender: gender,
            lifecycle: Lifecycle::Juvenile,
            infected: Infected::default(),
            roost: roost.clone()
        }
    }
}