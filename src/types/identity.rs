use super::*;

/// # RoostId
/// The RoostId of a roost site.
#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct RoostId {

    /// deserialized value from csv file
    _base: usize,

    /// the node index value provided by the graph
    _ni: Option<NodeIndex<KeySpace>>
}

impl RoostId {

    /// construct a roost id from a value
    pub fn new(value: usize) -> RoostId {
        RoostId {
            _base: value,
            _ni: None
        }
    }

    /// return true if no node index is set 
    pub fn not_set(&self) -> bool {
        self._ni.is_none()
    }

    /// set the node index value
    pub fn set_node_index(&mut self, index: NodeIndex<KeySpace>) {
        self._ni = Some(index);
    }

    /// promote by setting the base value to the index value
    pub fn promote(&mut self) {
        self._ni = Some(NodeIndex::new(self._base));
    }

    /// deserialized value read from csv file
    pub fn base(&self) -> usize {
        self._base
    }

    /// return the underlying value of the RoostId
    pub fn value(&self) -> usize {
        match self._ni {
            Some(v) => v.index(),
            None => self._base
        }
    }

    /// return the identity of the index
    pub fn index(&self) -> Option<NodeIndex<KeySpace>> {
        self._ni
    }
}

impl Default for RoostId {
    
    /// make a default
    fn default() -> RoostId {
        RoostId::from(0)
    }
}

impl From<NodeIndex<KeySpace>> for RoostId {

    /// from a node index
    fn from(nid: NodeIndex<KeySpace>) -> RoostId {
        let mut rid = RoostId::default();
        rid.set_node_index(nid);
        rid
    }
}

impl From<usize> for RoostId {

    /// from a node index
    fn from(id: usize) -> RoostId {
        RoostId::new(id)
    }
}

impl From<ProtoID> for RoostId {

    /// from a proto id 
    fn from(proto: ProtoID) -> RoostId {
        RoostId::from(proto.id)
    }
}

// prototypical helper struct for
// custom deserialization of ID
#[derive(Deserialize)]
struct ProtoID {
    #[serde(default)]
    #[serde(rename="roost")]
    id: usize,
}

/// deserialize a roost id value
pub fn deserialize_roost_id<'de, D>(deserializer: D) -> Result<RoostId, D::Error> 
    where D:Deserializer<'de> {

        // deserialize the usize stored in the csv file
        let proto = ProtoID::deserialize(deserializer)?;

        // return an RoostId
        Ok(RoostId::from(proto))
}