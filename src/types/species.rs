use super::*;

/// # SpeciesKey
/// Key for different species.
#[derive(Serialize,Component, Copy, Hash, Eq, PartialEq, Debug, Clone)]
#[storage(VecStorage)]
pub enum SpeciesKey {
    CORA,
    EPFU,
    LANO,
    LABO,
    LACI,
    LAIN,
    LASE,
    MYAU,
    MYGR,
    MYSE,
    MYLU,
    MYSO,
    NYHU,
    PISU,
}

/// # Species
/// The attributes of a specific species of bat.
/// These are loaded from a csv config file.
#[derive(Deserialize, Debug, Clone)]
pub struct Species {

    #[serde(deserialize_with="deserialize_species_key")]
    #[serde(rename="species")]
    pub name: SpeciesKey,

    // maximum age before death
    pub max_age: u8,

    // fecundity of the females
    pub fecundity: f32,

    // how many kilometers for migration during swarm
    #[serde(deserialize_with="deserialize_migration_distance")]
    pub migration_distance: Length,

    // infection rate when exposed to pathogen
    pub infection_rate: f32,

    // fidelity of species to winter hibernacula
    pub fidelity: f32,

    // percent of neighboring nodes that will be visited
    pub visitation_rate: f32,

    // disease mortality rate when infected (probability of dieing)
    pub disease_mortality_rate: f32,
}

/// deserialize a species key
pub fn deserialize_species_key<'de, D>(deserializer: D) -> Result<SpeciesKey, D::Error> 
    where D: Deserializer<'de> {

        // deserialize the species key to a String
        let string = String::deserialize(deserializer)?;

        // return a SpeciesKey
        match string.as_ref() {
            "cora" | "CORA" => Ok(SpeciesKey::CORA),
            "epfu" | "EPFU" => Ok(SpeciesKey::EPFU), 
            "lano" | "LANO" => Ok(SpeciesKey::LANO), 
            "labo" | "LABO" => Ok(SpeciesKey::LABO),
            "laci" | "LACI" => Ok(SpeciesKey::LACI),
            "lain" | "LAIN" => Ok(SpeciesKey::LAIN),
            "lase" | "LASE" => Ok(SpeciesKey::LASE),
            "myau" | "MYAU" => Ok(SpeciesKey::MYAU),
            "mygr" | "MYGR" => Ok(SpeciesKey::MYGR),
            "myse" | "MYSE" => Ok(SpeciesKey::MYSE),
            "mylu" | "MYLU" => Ok(SpeciesKey::MYLU),
            "myso" | "MYSO" => Ok(SpeciesKey::MYSO),
            "nyhu" | "NYHU" => Ok(SpeciesKey::NYHU),
            "pisu" | "PISU" => Ok(SpeciesKey::PISU),
            _ => Err(::serde::de::Error::custom("unknown species"))
        }
}

/// deserialize a migration distance
pub fn deserialize_migration_distance<'de, D>(deserializer: D) -> Result<Length, D::Error> 
    where D: Deserializer<'de> {

        // deserialize a migration distance
        let dist = f32::deserialize(deserializer)?;

        // return a migration distance
        Ok(Length::new::<kilometer>(dist))
}
