use super::*;

/// # Gender
/// Bats can be male or female.
#[derive(Component,Debug, Clone, Copy, is_enum_variant)]
#[storage(VecStorage)]
pub enum Gender {
    Male,
    Female
}


/// deserialize an gender value
pub fn deserialize_gender<'de, D>(deserializer: D) -> Result<Gender, D::Error> 
    where D:Deserializer<'de> {

        // get a string from the csv field
        let string = String::deserialize(deserializer)?;

        // return gender depending on value
        if string.starts_with("M") | string.starts_with("m") {
            Ok(Gender::Male)
        } else {
            Ok(Gender::Female)
        }
}