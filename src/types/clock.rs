use super::*;

/// # Season
/// The world can either be in Winter, Summer, or
/// Swarm season.
#[derive(Debug, Clone, is_enum_variant)]
pub enum Season {
    Winter,
    Summer,
    Swarm
}

impl Season {

    /// update the season to the next season
    pub fn update(&mut self) {
        *self = match *self {
            Season::Winter => Season::Summer,
            Season::Summer => Season::Swarm,
            Season::Swarm  => Season::Winter,
        };
    }
}

/// # Clock
/// A clock for the world. Will tell what season
/// the world is currently in and maintain a track
/// of the current run year of the model. It will
/// be stored as a resource for the ECS world.
#[derive(Debug, Clone)]
pub struct Clock {
    pub year: usize,
    pub season: Season
}

impl Clock {

    /// construct a new Clock
    pub fn new() -> Clock {
        // clock begins at end of swarm!
        let new = Clock {
            year: 0,
            season: Season::Swarm
        };

        // helpful message
        new.msg();

        // return new
        new
    }

    /// update the clock. If it is currently winter,
    /// the run year will also be updated on the switch
    /// to summer.
    pub fn update(&mut self) {
        self.season = match self.season {
            Season::Winter => {
                // update the year
                self.year += 1;
                // return the new season
                Season::Summer
            },
            Season::Summer => Season::Swarm,
            Season::Swarm  => Season::Winter,
        };

        // helpful message
        self.msg();
    }

    /// return true if currently in winter
    pub fn is_winter(&self) -> bool {
        self.season.is_winter()
    }

    /// return true if currently in summer
    pub fn is_summer(&self) -> bool {
        self.season.is_summer()
    }

    /// return true if currently in swarm
    pub fn is_swarm(&self) -> bool {
        self.season.is_swarm()
    }

    /// helpful messager
    fn msg(&self) {
        println!("Clock set to {0:#?} of year {1}.", self.season, self.year);
    }
}

impl Default for Clock {

    /// construct a default clock
    fn default() -> Clock {
        Clock::new()
    }
}