//! # Population
//! A sub-module that defines a population.
use super::*;

/// # Population
/// A population size resource.
#[derive(Clone, Debug, Default, Copy, Add, Sub)]
pub struct Population(u32);

impl Population {

    /// set the population
    pub fn set(&mut self, pop: u32) {
        self.0 = pop;
    }

    /// get the population
    pub fn get(&self) -> u32 {
        self.0
    }
}