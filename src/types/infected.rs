use super::*;

/// # Infected
/// An infection state flag.
#[derive(Serialize, Component,Debug, is_enum_variant, Clone, Copy)]
#[storage(VecStorage)]
pub enum Infected {
    // exposed to pathogen
    Exposed,
    // is currently infected
    Infected,
    // was infected but is now cleared of infection
    Recovered,
    // not infected
    None
}

impl Default for Infected {
    fn default() -> Infected {
        Infected::None
    }
}

impl Infected {

    /// infect the roost site
    pub fn expose(&mut self) {
        if self.is_none() {
            *self = Infected::Exposed;
        }
    }

    /// recover from the infection
    pub fn recover(&mut self) {
        if self.is_infected() {
            *self = Infected::Recovered;
        }
    }

    /// update exposed state to infection state
    pub fn update(&mut self) {
        if self.is_exposed() {
            *self = Infected::Infected;
        }
    }
}

/// deserialize infection state
pub fn deserialize_infected<'de, D>(deserializer: D) -> Result<Infected, D::Error> 
    where D: Deserializer<'de> {

        // deserialize infected state
        let infected = bool::deserialize(deserializer)?;

        // return infection state
        if infected {
            Ok(Infected::Infected)
        } else {
            Ok(Infected::None)
        }
}