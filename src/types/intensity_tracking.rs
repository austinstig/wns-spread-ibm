use std::time::Instant;
use super::*;

// # IntensityRecord
// a record for an intensity
#[derive(Serialize,Deserialize, Debug, Clone)]
struct IntensityRecord {
    time: u32,
}

/// # IntensityLog
/// Log the duration of the run of a System to a file.
pub struct IntensityLog {
    timer: Instant,
    filepath: PathBuf
}

impl IntensityLog {

    /// construct a new intensity log
    pub fn new<S: AsRef<str>>(base: &PathBuf, filename: S) -> IntensityLog {
        IntensityLog { timer: Instant::now(), filepath: base.join(filename.as_ref().to_string()) }
    }

    /// start timer
    pub fn start(&mut self) {
        self.timer = Instant::now();
    }
    
    /// end timer
    pub fn stop(&mut self) {
        // capture the elapsed time
        let delta_t = self.timer.elapsed();

        // read existing
        let mut records: Vec<IntensityRecord> = vec![];
        if let Ok(mut rdr) = Reader::from_path(self.filepath.clone()) {
            for result in rdr.deserialize() {
                match result {
                    Ok(rec) => { records.push( rec ); },
                    Err(_) => {  }
                }
            }
        }

        records.push(IntensityRecord { time: delta_t.subsec_nanos() });

        // open the writer builder
        if let Ok(mut wtr) = WriterBuilder::new()
                                    .has_headers(true)
                                    .delimiter(b',')
                                    .quote_style(QuoteStyle::Never)
                                    .from_path(self.filepath.clone()) {
            
            for rec in records.drain(..) {
                wtr.serialize(rec).ok();
            }
        }
        
    }
}