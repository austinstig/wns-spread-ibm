//! # RoostSite
//! A sub-module that defines a roost site.
use super::*;

/// # RoostSite
/// Roost sites are locations that bats can 
/// occupy. Their attributes are defined in a
/// configuration csv file.
#[derive(Deserialize, Debug, Clone)]
pub struct RoostSite {

    /// id value of roost site
    #[serde(flatten)]
    #[serde(default)]
    #[serde(deserialize_with="deserialize_roost_id")]
    pub id: RoostId,

    /// location of the roost site
    #[serde(flatten)]
    #[serde(deserialize_with="deserialize_site")]
    pub location: Site,

    // county name where site resides
    // FIPS: is a five digit code. The first
    // two digits are the state code, the remaining
    // three digits are the county code.
    pub fips: u32,

    /// maximum population capacity of the roost site
    #[serde(flatten)]
    pub capacity: Capacity,

    /// current infection status of the roost site
    #[serde(rename="init_infection_state")]
    #[serde(deserialize_with="deserialize_infected")]
    pub infected: Infected,

    /// mean temperature of roost site
    #[serde(flatten)]
    pub mean_temp: Temperature,

    /// a liklihood multiplier for visitiation rate
    #[serde(default="default_float_one")]
    pub visitation_multiplier: f32,
}

impl RoostSite {

    /// compute the haversine distance between roost sites
    /// return in units of Meters.
    pub fn distance(&self, other: &RoostSite) -> Length {

        // compute haversine distance between roost sites
        let distance = self.location.0.haversine_distance(&other.location.0);

        // return distances as a typed length
        Length::new::<meter>(distance)
    }
}

/// The default value for visitation multiplier
fn default_float_one() -> f32 { 1_f32 }