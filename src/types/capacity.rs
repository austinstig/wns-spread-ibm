use super::*;

/// # Capacity
/// The maximum population size that is
/// supported at a given roost site.
#[derive(Component,Deserialize, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Capacity {
    /// increment the current population count
    #[serde(default)]
    _current: u32,
    /// maximum population the roost can sustain
    pub max_pop: u32,
}

impl Capacity {

    /// reset the current capacity count
    pub fn reset(&mut self) {
        self._current = 0;
    }

    /// increment the current capacity count
    pub fn incr(&mut self) {
        self._current += 1;
    }

    /// decrement the current capacity count
    pub fn decr(&mut self) {
        self._current -= 1;
    }

    /// the current count
    pub fn count(&self) -> u32 {
        self._current
    }
}