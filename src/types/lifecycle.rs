use super::*;

/// # Lifecycle
/// A type that represent three distinct lifecycle
/// stages of an individual bat. Juvinille, Mature,
/// and deceased.
#[derive(Component,Debug, Clone, Copy, is_enum_variant)]
#[storage(VecStorage)]
pub enum Lifecycle {
    // is 0-1 yrs old
    Juvenile,
    // stores an age value
    Mature(u8),
    // is decased
    Deceased
}

impl Lifecycle {

    /// evaluate for death by natural causes.
    pub fn evaluate(&mut self, species: &Species, rand: f32) {
        // determine probability of death
        if let Lifecycle::Mature(current) = self {
            // kill the bat if it's too old
            if *current == species.max_age { 
                self.kill(); 

            // apply death as age approaches max
            } else {

                // compute probability of death 1/(max)^2 * (current)*2
                let p_death = (1f32/(species.max_age as f32).powf(2f32)) * (*current as f32).powf(2f32);

                // if rand <= pDeath then kill bat
                if rand <= p_death {
                    self.kill();
                }
            }
        }
    }

    /// return true if alive
    pub fn is_alive(&self) -> bool {
        self.is_juvenile() || self.is_mature()
    }

    /// kill the bat
    pub fn kill(&mut self) {
        debug!("kill the bat!");
        *self = Lifecycle::Deceased;
    }

    /// update the age of the bat by 1 year. Does
    /// nothing if bat is deceased. If bat it juvenile
    /// it converts it to a mature bat.
    pub fn update(&mut self) {
        // update the current age -- do nothing if deceased
        *self = match *self {
            Lifecycle::Juvenile => Lifecycle::Mature(0),
            Lifecycle::Mature(a) => Lifecycle::Mature(a+1),
            Lifecycle::Deceased => Lifecycle::Deceased
        };
    }
}

impl Default for Lifecycle {

    /// return a default lifecycle
    fn default() -> Lifecycle {
        Lifecycle::Juvenile
    }
}


/// deserialize a lifecycle from an age value
pub fn deserialize_lifecycle<'de, D>(deserializer: D) -> Result<Lifecycle, D::Error> 
    where D:Deserializer<'de> {

    // get a value for age
    let v = u8::deserialize(deserializer)?;

    // return the age
    Ok(if v == 0 {
        Lifecycle::Juvenile
    } else {
        Lifecycle::Mature(v)
    })
}