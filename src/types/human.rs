//! # HumanMovement
//! A sub-module that defines human movement resource.
use super::*;

/// # HumanMovement
/// Human movement rate applied to human population to
/// move between two random caves.
#[derive(Deserialize, Default, Debug, Clone)]
pub struct HumanMovement {
    // human population size
    // that will spread disease
    pub pop: u64,
}

impl HumanMovement {

    /// construct a new HumanMovement resource
    pub fn new(pop: u64) -> HumanMovement {
        HumanMovement { pop }
    }
}