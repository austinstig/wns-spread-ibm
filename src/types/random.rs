//! # Random
//! A sub-module that defines random types
//! to support the stochastic properties
//! of the model.
use super::*;

/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomReproduction1(f32);

impl RandomReproduction1 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}

/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomReproduction2(f32);

impl RandomReproduction2 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn sample(&self, count: usize) -> usize {
        // determine the closest integer
        let i = (self.0 * count as f32).round();
        i as usize
    }
}


/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomReproduction3(bool);

impl RandomReproduction3 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<bool>();
    }
    pub fn value(&self) -> bool {
        self.0
    }
}


/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomMovement1(f32);

impl RandomMovement1 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}


/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomMovement2(f32);

impl RandomMovement2 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn sample(&self, count: usize) -> usize {
        // determine the closest integer
        let i = (self.0 * count as f32).round();
        i as usize
    }
}


/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomMovement3(f32);

impl RandomMovement3 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}

/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomMovement4(f32);

impl RandomMovement4 {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}

/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomDiseaseMortality(f32);

impl RandomDiseaseMortality {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}


/// # Random Number
#[derive(Component,Deserialize, Debug, Clone, Default)]
#[storage(VecStorage)]
pub struct RandomAgeMortality(f32);

impl RandomAgeMortality {
    pub fn update(&mut self, rng: &mut IsaacRng) {
        self.0 = rng.gen::<f32>();
    }
    pub fn value(&self) -> f32 {
        self.0
    }
}