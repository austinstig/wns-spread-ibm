//! # Location
//! A sub-module for defining locations.
use super::*;

/// # Location
/// A geographic location. Used as a
/// field to identify the location of
/// roost sites. This type is used
/// during the deserialization of a
/// point location.
#[derive(Deserialize, Debug, Clone, Copy)]
pub struct Location {

    /// latitude component of location
    #[serde(rename="lat")]
    latitude: f32,

    /// longitude component of location
    #[serde(rename="lon")]
    longitude: f32,
}

impl From<Location> for Point<f32> {

    /// convert the location into a geospatial point
    fn from(location: Location) -> Point<f32> {
        Point::new(location.longitude, location.latitude)
    }
}

/// # Site
/// A roost site location suitable for ECS storage.
#[derive(Component, Debug, Clone, Copy)]
#[storage(VecStorage)]
pub struct Site(pub Point<f32>);

impl From<Point<f32>> for Site {

    /// convert from a location into a site location
    fn from(point: Point<f32>) -> Site {
        Site(point)
    }
}

/// deserialize a point by first passing it though a Location type for deserialization
pub fn deserialize_site<'de, D>(deserializer: D) -> Result<Site, D::Error> 
    where D:Deserializer<'de> {

        // deserialize a location from the deserializer
        let location: Location = Location::deserialize(deserializer)?;

        // convert the location into a Point
        Ok(Site(location.into()))
}