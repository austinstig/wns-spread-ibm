//! # CSV
//! A module for CSV IO.
use super::*;

/// read a configuration file for parsing
pub fn read_csv<T: for<'d> Deserialize<'d>, 
                P: AsRef<Path>>(filename: P) -> Result<Vec<T>, WNSError> {

    // construct a pathbuf to the path
    let path: PathBuf = PathBuf::from(filename.as_ref());

    // open the file for reading
    let f = File::open(path)?;

    // read the file into the csv reader
    let mut rdr = csv::Reader::from_reader(f);

    // output vector
    let mut output = vec![];

    // load the records from the reader
    for row in rdr.deserialize() {

        // parse the record into the Type
        let record: T = row?;

        // add the parsed result to
        // the output list of records
        output.push(record);
    }

    // return the set of records
    Ok(output)
}

