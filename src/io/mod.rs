//! # IO
//! A module that contains useful input/output
//! capabilities
use super::*;

// link modules
mod csv;


/// load the roost sites from the roost config
pub fn load_roost_sites<P: AsRef<Path>>(roost_csv: P) -> Result<Vec<RoostSite>, WNSError> {

    // load the roost site information
    let mut roosts: Vec<RoostSite> = csv::read_csv(roost_csv)?;

    // show the roosts
    info!("loaded {} roosts.", roosts.len());

    // set base of roosts
    for (idx, roost) in roosts.iter_mut().enumerate() {
        roost.id = RoostId::new(idx);
    }

    // return the roosts
    Ok(roosts)
}

/// load the bats from the bat config
pub fn load_bats<P: AsRef<Path>>(bats_csv: P) -> Result<Vec<Bat>, WNSError> {

    // load the bat information
    let bats: Vec<Bat> = csv::read_csv(bats_csv)?;

    // show the bats
    info!("loaded {} bats.", bats.len());

    // return the bats
    Ok(bats)
}

/// load the species from the species config
pub fn load_species<P: AsRef<Path>>(species_csv: P) -> Result<Vec<Species>, WNSError> {

    // load the species information
    let species: Vec<Species> = csv::read_csv(species_csv)?;

    // show the species
    info!("loaded {} species.", species.len());

    // return the species
    Ok(species)
}


/// # Configuration
/// The configuration system for the wns model
pub struct Configuration {

    // true to use concurrent dispatch mode
    pub dispatch: bool,

    // maximum population the model can sustatin
    pub max_pop: u32,

    // number of years to simulate
    pub years: u32,

    // seed value for ISAAC Random Number Genderator
    pub seed: u64,

    // roost csv file
    pub roost_csv: PathBuf,

    // bats csv file
    pub bats_csv: PathBuf,

    // species csv file
    pub species_csv: PathBuf,

    // output logging directory
    pub output_directory: PathBuf,

    // set a thread count to use <-- if 0 use default number based on rayon
    pub thread_count: usize,

    // human population that can spread WNS
    pub human_pop: u64,
}

impl Configuration {

    pub fn load_config<P: AsRef<Path>>(filename: P) -> Result<Configuration,WNSError> {
        // set the settings for the application
        let mut settings = config::Config::default();
        settings
            // load settings from any configuration files
            .merge(config::File::from(PathBuf::from(filename.as_ref())))
            .expect("No configuration file found!")
            // load settings from any environments prefixed with "WNS_"
            .merge(config::Environment::with_prefix("WNS"))
            .expect("Failed to load from environment");

        // parse the configuration file
        let dispatch: bool = settings.get("dispatch")?;
        let max_pop: u32 = settings.get("max_pop")?;
        let years: u32 = settings.get("years")?;
        let seed: u64 = settings.get("seed")?;
        let roost_csv: PathBuf = settings.get("roost_csv")?;
        let bats_csv: PathBuf = settings.get("bats_csv")?;
        let species_csv: PathBuf = settings.get("species_csv")?;
        let output_directory: PathBuf = settings.get("output_directory")?;
        let thread_count: usize = settings.get("thread_count")?;
        let human_pop: u64 = settings.get("human_pop")?;

        if !output_directory.exists() {
            create_dir_all(&output_directory);
        }

        // construct Configuration
        Ok(Configuration {
            dispatch,
            max_pop,
            years,
            seed,
            roost_csv,
            bats_csv,
            species_csv,
            output_directory,
            thread_count,
            human_pop
        })
    }
}