use super::*;

/// # DiseaseInfectionSystem
/// A system that updates the infection state of the bats.
pub struct DiseaseInfectionSystem {
    lg: IntensityLog,
}

impl DiseaseInfectionSystem {
    pub fn new(outdir: &PathBuf) -> DiseaseInfectionSystem {
        DiseaseInfectionSystem {
            lg: IntensityLog::new(outdir, "DiseaseInfectionSystem.csv")
        }
    }
}

impl<'a> System<'a> for DiseaseInfectionSystem {

    type SystemData = (Read<'a, Clock>,
                       Entities<'a>, 
                       WriteStorage<'a, Infected>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (clock, entities, mut infected) = data;

        // update the disease state if it is winter
        if clock.is_winter() {
            // loop over entities and infection states
            (&entities, &mut infected).par_join()
                // update the infection states
                .for_each(|(_,state)| state.update());
        }

        self.lg.stop();
    }
}
