use super::*;

/// # Clock System
/// Update with global clock in the runtime.
pub struct ClockSystem;

impl<'a> System<'a> for ClockSystem {

    type SystemData = Write<'a, Clock>;

    fn run(&mut self, data: Self::SystemData) {

        // update the global clock resource
        let mut clock = data;
        clock.update();
    }
}

/// # Clock System (for dispatcher)
/// Update with global clock in the runtime.
pub struct ClockToWinterSystem;

impl<'a> System<'a> for ClockToWinterSystem {

    type SystemData = Write<'a, Clock>;

    fn run(&mut self, data: Self::SystemData) {

        // update the global clock resource
        let mut clock = data;
        clock.update();
    }
}

/// # Clock System (for dispatcher)
/// Update with global clock in the runtime.
pub struct ClockToSummerSystem;

impl<'a> System<'a> for ClockToSummerSystem {

    type SystemData = Write<'a, Clock>;

    fn run(&mut self, data: Self::SystemData) {

        // update the global clock resource
        let mut clock = data;
        clock.update();
    }
}

/// # Clock System (for dispatcher)
/// Update with global clock in the runtime.
pub struct ClockToSwarmSystem;

impl<'a> System<'a> for ClockToSwarmSystem {

    type SystemData = Write<'a, Clock>;

    fn run(&mut self, data: Self::SystemData) {

        // update the global clock resource
        let mut clock = data;
        clock.update();
    }
}