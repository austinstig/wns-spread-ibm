use super::*;

/// # Reproduction System
/// A system that is used for reproduction of the population
pub struct ReproductionSystem {
    capacity: u32,
    lg: IntensityLog,
}

impl ReproductionSystem {

    /// construct a new reproduction system
    pub fn new(cap: u32, outdir: &PathBuf) -> ReproductionSystem {
        ReproductionSystem {
            capacity: cap,
            lg: IntensityLog::new(outdir, "ReproductionSystem.csv")
        }
    }
}

impl<'a> System<'a> for ReproductionSystem {

    type SystemData = (Read<'a, Landscape>, 
                       Read<'a, Population>,
                       Read<'a, Clock>,
                       Read<'a, SpeciesTable>, 
                       Read<'a, LazyUpdate>, 
                       Entities<'a>, 
                       ReadStorage<'a, RandomReproduction1>, 
                       ReadStorage<'a, RandomReproduction2>, 
                       ReadStorage<'a, RandomReproduction3>, 
                       ReadStorage<'a, Gender>, 
                       ReadStorage<'a, SpeciesKey>, 
                       ReadStorage<'a, Lifecycle>, 
                       ReadStorage<'a, RoostId>);

    fn run(&mut self, data: Self::SystemData) {

        self.lg.start();

        // parse out the data
        let (landscape, master_pop, clock, sp_table, lazy, entities, rand1, rand2, rand3, gender, spkey, age, roost_ids) = data;

        // artificially limit the maximum population size!
        // determine the instantaneous growth rate, treating species rate as maximum rate
        let rate: f32 = ((self.capacity - master_pop.get()) as f32) / self.capacity as f32;

        // only produce bats during summer maternity season
        if clock.is_summer() {

            // 1) loop over the entities.
            (&*entities, &rand1, &rand2, &rand3, &gender, &age, &spkey, &roost_ids).par_join() 
                // 2) identify the female bats
                .filter(|(_,_,_,_,sex,_,_,_)| sex.is_female())
                // 3) identify the mature bats
                .filter(|(_,_,_,_,_,lf,_,_)| lf.is_mature())
                .for_each(|(_,r1,r2,r3,_,_,key,rid)| {
                    // 4) determine the species fecundity
                    if let Some(ref sp) = sp_table.get(&key) {
                        // 5) select a random number (if <= fecundity then reproduce)
                        let birth_rate = if rate < sp.fecundity { rate } else { sp.fecundity };  // <-- applies logarithmic growth constraint based on max pop size

                        if r1.value() <= birth_rate {
                            // 6) child will reside within migration distance 
                            //    of mother's roost id
                            if let Some(idx) = rid.index() {
                                let count = landscape.edges(idx).count();
                                let start: usize = r2.sample(count);
                                // if len() + 1 then use mother's roost for start
                                let roost: RoostId = if start == count {
                                    rid.clone()
                                } else {
                                    RoostId::from(landscape.neighbors(idx).nth(start).unwrap())
                                };

                                // determine the gender of the child
                                let gender = match r3.value() {
                                    true => Gender::Male,
                                    false=> Gender::Female
                                };

                                // construct a new bat, add it to the world, and assign it a roost
                                let new = Bat::birth(sp.name.clone(),gender, &roost );

                                //info!("lazy make an entity (added on world.maintain())");
                                lazy.create_entity(&entities)
                                    .with(new.species)
                                    .with(new.gender)
                                    .with(new.infected)
                                    .with(new.lifecycle)
                                    .with(new.roost)
                                    .with(RandomMovement1::default())
                                    .with(RandomMovement2::default())
                                    .with(RandomMovement3::default())
                                    .with(RandomMovement4::default())
                                    .with(RandomReproduction1::default())
                                    .with(RandomReproduction2::default())
                                    .with(RandomReproduction3::default())
                                    .with(RandomDiseaseMortality::default())
                                    .with(RandomAgeMortality::default())
                                    .build();
                            }
                        }
                    }
                });
        }
        self.lg.stop();
    }
}
