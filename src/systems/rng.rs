use super::*;

/// # RNG System
/// Update all of the random numbers for all of the entities.
pub struct RNGSystem {
    rng: IsaacRng,
    lg: IntensityLog,
}

impl RNGSystem {
    /// construct a new RNG System
    pub fn new(seed: u64, outdir: &PathBuf) -> RNGSystem {
        RNGSystem { 
            rng: IsaacRng::seed_from_u64(seed),
            lg: IntensityLog::new(outdir, "RNGSystem.csv") 
        }
    }
}

impl<'a> System<'a> for RNGSystem {

    type SystemData = (Entities<'a>, 
                       WriteStorage<'a, RandomReproduction1>,
                       WriteStorage<'a, RandomReproduction2>,
                       WriteStorage<'a, RandomReproduction3>,
                       WriteStorage<'a, RandomMovement1>,
                       WriteStorage<'a, RandomMovement2>,
                       WriteStorage<'a, RandomMovement3>,
                       WriteStorage<'a, RandomMovement4>,
                       WriteStorage<'a, RandomDiseaseMortality>,
                       WriteStorage<'a, RandomAgeMortality>,
        );

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();


        // parse out the data
        let (entities, mut r1, mut r2, mut r3, mut r4, mut r5, mut r6, mut r7, mut r8, mut r9) = data;

        // iterate over all of the entities and update their random numbers
        for (_, v1, v2, v3, v4, v5, v6, v7, v8, v9) in (&*entities, &mut r1, &mut r2, &mut r3, &mut r4, &mut r5, &mut r6, &mut r7, &mut r8, &mut r9).join() {
            v1.update(&mut self.rng);
            v2.update(&mut self.rng);
            v3.update(&mut self.rng);
            v4.update(&mut self.rng);
            v5.update(&mut self.rng);
            v6.update(&mut self.rng);
            v7.update(&mut self.rng);
            v8.update(&mut self.rng);
            v9.update(&mut self.rng);
        }

        self.lg.stop();
    }
}