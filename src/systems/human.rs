use super::*;

/// # Human Movement System
/// A system that is used to represent human transmission of WNS.
pub struct HumanMovementSystem {
    rng: IsaacRng,
    lg: IntensityLog
}

impl HumanMovementSystem {
    /// construct a new Human Movement System
    pub fn new(seed: u64, outdir: &PathBuf) -> HumanMovementSystem {
        HumanMovementSystem { 
            rng: IsaacRng::seed_from_u64(seed),
            lg: IntensityLog::new(outdir, "HumanMovementSystem.csv")
        }
    }
}

impl<'a> System<'a> for HumanMovementSystem {

    type SystemData = (Write<'a, Landscape>,
                       Read<'a, HumanMovement>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (mut landscape, humans) = data;

        // determine the number of humans to move
        let n: u64 = humans.pop;

        // for each human select two random nodes on the landscape
        for _ in 0u64..n {
            // select a source roost for the human
            // and determine if it is currently infected.
            let src_infected = match landscape.node_indices().choose(&mut self.rng) {
                Some(r) => match landscape.node_weight(r) {
                    Some(roost) => roost.infected.is_infected(),
                    None => false
                },
                None => false
            };
            // if src is infected then infect the destination
            if src_infected {
                // expose the destination roost to potential infection
                if let Some(r) = landscape.node_indices().choose(&mut self.rng) {
                    if let Some(roost) = landscape.node_weight_mut(r) {
                        roost.infected.expose();
                    }
                }
            }
        }
        self.lg.stop();
    }
}