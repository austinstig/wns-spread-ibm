use super::*;

/// # ShedSporeSystem
/// A system that is used to infect roosts when bats arrive with infection.
pub struct ShedSporeSystem {
    lg: IntensityLog,
}

impl ShedSporeSystem {
    pub fn new(outdir: &PathBuf) -> ShedSporeSystem {
        ShedSporeSystem {
            lg: IntensityLog::new(outdir, "ShedSporeSystem.csv")
        }
    }
}

impl<'a> System<'a> for ShedSporeSystem {

    type SystemData = (Read<'a, Clock>,
                       Write<'a, Landscape>, 
                       Entities<'a>, 
                       ReadStorage<'a, RoostId>,
                       ReadStorage<'a, Infected>);

    fn run(&mut self, data: Self::SystemData) {

        self.lg.start();

        // parse out the data
        let (clock, mut landscape, entities, roosts, infection) = data;

        // only shed spores at the beginning of winter
        if clock.is_winter() {

            // iterate over the landscape nodes
            landscape.node_weights_mut()
                // ignore any roost sites where the mean temp is not conducive for Pd growth.
                // temperature is given in units of celsius
                .filter(|n| n.mean_temp.ok_for_pathogen())
                // only shed spores into uninfected sites <-- (for speedup)
                .filter(|n| n.infected.is_none())
                // determine if any present bats are infected
                .for_each(|node| {
                    if (&entities, &roosts, &infection).par_join()
                        .filter(|(_,rid,_)| rid.value() == node.id.value())
                        .any(|(..,state)| state.is_exposed() || state.is_infected()) {

                            // if so then infect the node
                            node.infected.expose();
                    }
                });
        }

        self.lg.stop();
    }
}

/// # GrowPathogenSystem
/// A system that will grow the pathogen on the landscape. Roosts
/// that have been exposed will become fully infected.
pub struct GrowPathogenSystem {
    lg: IntensityLog,
}

impl GrowPathogenSystem {
    pub fn new(outdir: &PathBuf) -> GrowPathogenSystem {
        GrowPathogenSystem {
            lg: IntensityLog::new(outdir, "GrowPathogenSystem.csv")
        }
    }
}


impl<'a> System<'a> for GrowPathogenSystem {

    type SystemData = (Read<'a, Clock>,
                       Write<'a, Landscape>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (clock, mut landscape) = data;

        // pathogen is only grown during winter
        if clock.is_winter() {
            // iterate over node weights
            landscape.node_weights_mut()
                // update each node's infection state
                .for_each(|n| { n.infected.update(); });
        }

        self.lg.stop();
    }
}