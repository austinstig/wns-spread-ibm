use super::*;

/// # Movement System
/// A system that is used for the movement of bats to 
/// neighboring roosts and be exposed to Pd. It will also
/// identify a new winter hibernacula given a fidelity metric
/// for each species.
pub struct MovementSystem {
    lg: IntensityLog,
}

impl MovementSystem {
    pub fn new(outdir: &PathBuf) -> MovementSystem {
        MovementSystem {
            lg: IntensityLog::new(outdir, "MovementSystem.csv")
        }
    }
}

impl<'a> System<'a> for MovementSystem {

    type SystemData = (Read<'a, Landscape>, 
                       Read<'a, Clock>,
                       Read<'a, SpeciesTable>, 
                       Entities<'a>, 
                       ReadStorage<'a, SpeciesKey>, 
                       ReadStorage<'a, RandomMovement1>, 
                       ReadStorage<'a, RandomMovement2>, 
                       ReadStorage<'a, RandomMovement3>, 
                       ReadStorage<'a, RandomMovement4>, 
                       WriteStorage<'a, Infected>,
                       WriteStorage<'a, RoostId>);

    fn run(&mut self, data: Self::SystemData) {

        self.lg.start();

        // parse out the data
        let (landscape, clock, sp_table, entities, spkey, rand1, rand2, rand3, rand4, mut is_infected, mut roost_ids) = data;

        // only move bats during swarm
        if let Season::Swarm = clock.season {
            // evaluate each entity
            (&*entities, &spkey, &rand1, &rand2, &rand3, &rand4, &mut is_infected, &mut roost_ids).par_join()
                .for_each(|(_, key, r1, r2, r3, r4, exposed, rid)| {

                // get the species data
                if let Some(ref sp) = sp_table.get(&key) {
                    // get the roost id index
                    let idx = rid.index().unwrap();

                    // get the number of neighbors
                    let count = landscape.edges(idx).count();

                    // access the set of neighbors
                    let neighbor_roosts = landscape.neighbors(idx);

                    // determine the number to visit
                    let mut visit_cnt = (sp.visitation_rate * count as f32).round() as usize;
                    
                    // compute visition of local sites
                    let num_sites = landscape.node_count();
                    let local_multiple: f32 = (count as f32) / (num_sites as f32);
                    let mut exposure: bool = false;
                    let mut iterator = neighbor_roosts.filter_map(|i| landscape.node_weight(i));
                    while visit_cnt > 0 && !exposure {
                        match iterator.next() {
                            Some(roost) => if r3.value() <= (roost.visitation_multiplier * local_multiple) {
                                visit_cnt -= 1;
                                if roost.infected.is_infected() && r4.value() <= sp.infection_rate {
                                    exposure = true;
                                }
                            },
                            None => { break; }
                        }
                    }
                    
                    // if bat was exposed during neighbor walk
                    // then update its infection state
                    if exposure { 
                        exposed.expose(); 
                    }

                    // next determine if bat will exibit fidelity to its
                    // winter hibernacula.
                    if r1.value() > sp.fidelity {
                        // get the new roost index
                        let new_roost_idx: usize = r2.sample(count);
                        // get the new roost
                        if let Some(new_roost) = landscape.neighbors(idx).nth(new_roost_idx) {
                            // get the id of the new roost
                            let new_roost_id = RoostId::from(new_roost.clone());
                            debug!("Update Roost Id to {:#?}", new_roost_id.value());
                            // update the roost id
                            *rid = new_roost_id;
                        }
                    }
                }
            });

            // loop over all of the entities to determine if they will exhibit fidelity
            // to their winter roosts. If they do not exhibit fidelity then update their
            // roost id value.
            

        }
        self.lg.stop();
    }
}
