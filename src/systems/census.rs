use super::*;

/// # Census System
/// A system that is used to count the number of bats at each roost site.
pub struct CensusSystem {
    lg: IntensityLog,
}

impl CensusSystem {
    pub fn new(outdir: &PathBuf) -> CensusSystem {
        CensusSystem {
            lg: IntensityLog::new(outdir, "CensusSystem.csv")
        }
    }
}

impl<'a> System<'a> for CensusSystem {

    type SystemData = (Write<'a, Population>, 
                       Write<'a, Landscape>, 
                       Entities<'a>, 
                       ReadStorage<'a, RoostId>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (mut master_pop, mut landscape, entities, roost_ids) = data;

        // iterate over the landscape nodes
        let mut pop: u32 = 0;
        for node in landscape.node_weights_mut() {

            // reset the current capacity
            node.capacity.reset();

            // determine the new current capacity
            for (_, rid) in (&entities, &roost_ids).join() {

                // roost id values match
                if rid.value() == node.id.value() {

                    // increment the count
                    node.capacity.incr();
                }
            }

/*
            // print some progress message
            let msg = format!("node[{0}] \"{1}\"\tpopulation: {2}", node.id.value(), node.fips, node.capacity.count());
            match node.infected {
                Infected::Exposed => { println!("{}",msg.yellow()); }
                Infected::Infected => { println!("{}",msg.red()); }
                Infected::Recovered => { println!("{}",msg.blue()); }
                Infected::None => { println!("{}",msg.green()); }
            };
*/
            pop += node.capacity.count();

        }

        // update the population resource
        master_pop.set(pop);

        self.lg.stop();
        
        // print the population size
        println!("\t\t\t\tTOTAL: {}", pop);
    }
}