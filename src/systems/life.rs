use super::*;

/// # SenesenceSystem
/// A system that is used to age the bats from juvenile
/// to mature and increment the annual age of the mature
/// bats.
pub struct SenesenceSystem {
    lg: IntensityLog
}

impl SenesenceSystem {
    pub fn new(outdir: &PathBuf) -> SenesenceSystem {
        SenesenceSystem {
            lg: IntensityLog::new(outdir, "SenesenceSystem.csv")
        }
    }
}

impl<'a> System<'a> for SenesenceSystem {

    type SystemData = (Read<'a, Clock>, 
                       Entities<'a>, 
                       WriteStorage<'a, Lifecycle>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (clock, entities, mut lifecycle) = data;

        // convert the juviniles to adults when they exit swarm
        if clock.is_winter() {
            (&entities, &mut lifecycle).par_join()
                .filter(|(_, lf)| lf.is_juvenile())
                .for_each(|(_, lf)| lf.update());
        }

        // update the age of all mature bats 
        // on the transition to summer
        if clock.is_summer() {
            (&entities, &mut lifecycle).par_join()
                .for_each(|(_, lf)| lf.update());
        }
        self.lg.stop();

    }
}

/// # AgeMortalitySystem
/// A system used to apply age related mortatlity to bats.
/// If the bat's age is >= the max species age it is killed.
/// A probability that the bat will die younger is determined
/// from the formula: p = 1/(max)^2 * (age)^2
pub struct AgeMortalitySystem {
    lg: IntensityLog
}

impl AgeMortalitySystem {
    pub fn new(outdir: &PathBuf) -> AgeMortalitySystem {
        AgeMortalitySystem {
            lg: IntensityLog::new(outdir, "AgeMortalitySystem.csv")
        }
    }
}

impl<'a> System<'a> for AgeMortalitySystem {

    type SystemData = (Read<'a, Clock>, 
                       Read<'a, SpeciesTable>, 
                       Entities<'a>, 
                       ReadStorage<'a, RandomAgeMortality>,
                       ReadStorage<'a, SpeciesKey>, 
                       WriteStorage<'a, Lifecycle>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (clock, table, entities, rand, species_key, mut lifecycle) = data;

        // age mortality is only applied on the emergence from winter to summer
        if clock.is_summer() {

            // loop over the living entities
            // access the species information
            // determine if the bat needs to 
            // be deceased for age related
            // conditions
            (&*entities, &rand, &species_key, &mut lifecycle)
                .par_join()
                .for_each(|(_, r, key, lf)| {

                    // get access to species attributes
                    if let Some(ref sp) = table.get(&key) {
                        lf.evaluate(sp, r.value());
                    } else {
                        // log error
                        error!("no species found for key: {:?}", key);
                        // panic on the failure
                        panic!("no species found for key: {:?}", key);
                    }
                });
        }
        self.lg.stop();
    }
}

/// # ResourceStarvationMortalitySystem
/// A system that causes mortality events when roosts exceed
/// there capacity. These are mortality events due to population
/// pressures on the landscape.
pub struct ResourceStarvationMortalitySystem {
    lg: IntensityLog
}

impl ResourceStarvationMortalitySystem {
    pub fn new(outdir: &PathBuf) -> ResourceStarvationMortalitySystem {
        ResourceStarvationMortalitySystem {
            lg: IntensityLog::new(outdir, "ResourceStarvationMortalitySystem.csv")
        }
    }
}


impl<'a> System<'a> for ResourceStarvationMortalitySystem {

    type SystemData = (Read<'a, Landscape>, 
                       Entities<'a>, 
                       WriteStorage<'a, Lifecycle>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (landscape, entities, mut ages) = data;
        
        // iterate over the landscape nodes
        for nidx in landscape.node_indices() {

            if let Some(w) = landscape.node_weight(nidx) {

                // determine the current capacity
                let current = w.capacity.count();

                // determine the maximum capacity
                let maximum = w.capacity.max_pop;

                // determine the population level over maximum
                if current > maximum {
                    (&*entities, &mut ages)
                        .join()
                        .take((current-maximum) as usize)
                        // kill the first set of bats over the
                        // maximum capacity for this node
                        .for_each(|(_,lf)| lf.kill());
                }
            }
        }
        self.lg.stop();
    }
}

/// # DiseaseMortalitySystem
/// A system that is used to apply disease mortality
/// to bats that are infected with WNS.
pub struct DiseaseMortalitySystem {
    lg: IntensityLog
}

impl DiseaseMortalitySystem {
    pub fn new(outdir: &PathBuf) -> DiseaseMortalitySystem {
        DiseaseMortalitySystem {
            lg: IntensityLog::new(outdir, "DiseaseMortalitySystem.csv")
        }
    }
}

impl<'a> System<'a> for DiseaseMortalitySystem {

    type SystemData = (Read<'a, SpeciesTable>, 
                       Read<'a, Clock>, 
                       Entities<'a>, 
                       ReadStorage<'a, RandomDiseaseMortality>,
                       WriteStorage<'a, Lifecycle>,
                       ReadStorage<'a, SpeciesKey>,
                       ReadStorage<'a, Infected>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (table, clock, entities, rand, mut lifecycle, species, infected) = data;

        // only apply disease mortality during winter
        if clock.is_winter() {

            // iterate over the entities and kill off
            // and bats that are infected based on their
            // disease mortality rate for the species.
            let killed_off: usize = (&entities, &mut lifecycle, &species, &rand, &infected).par_join()
                // only evaluated infected bats
                .filter(|(..,state)| state.is_infected())
                .map(|(_,lf,key,r,_)| {
                    // get the species from the species table
                    if let Some(ref sp) = table.get(&key) {
                        // get the disease mortality rate
                        if r.value() <= sp.disease_mortality_rate {
                            lf.kill();
                            1 // return 1
                        } else { 0 }
                    } else { 0 }
                })
                .sum();

            // print a console message
            println!("\t\t\t\tDISEASED DEATHS: {}", killed_off);
        }
        self.lg.stop();
    }
}


/// # RemovalSystem
/// A system that removes all deceased bats
/// from the ECS world if they are marked as
/// deceased.
pub struct RemovalSystem {
    lg: IntensityLog
}

impl RemovalSystem {
    pub fn new(outdir: &PathBuf) -> RemovalSystem {
        RemovalSystem {
            lg: IntensityLog::new(outdir, "RemovalSystem.csv")
        }
    }
}
impl<'a> System<'a> for RemovalSystem {

    type SystemData = (Read<'a, LazyUpdate>,
                       Entities<'a>,
                       ReadStorage<'a, Lifecycle>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (lazy, entities, lifecycle) = data;

        // loop over the entities filtering by those
        // entities that are currently deceased.
        (&entities, &lifecycle).par_join()
            .filter(|(_, lf)| lf.is_deceased())
            .for_each(|(ent,_)| {
                // lazily remove the entity.
                // it will be fully removed
                // on successive call to 
                // 'world.maintain()'
                lazy.remove::<SpeciesKey>(ent);
                lazy.remove::<RoostId>(ent);
                lazy.remove::<Lifecycle>(ent);
                lazy.remove::<Infected>(ent);
                lazy.remove::<Gender>(ent);
                lazy.remove::<RandomMovement1>(ent);
                lazy.remove::<RandomMovement2>(ent);
                lazy.remove::<RandomMovement3>(ent);
                lazy.remove::<RandomMovement4>(ent);
                lazy.remove::<RandomReproduction1>(ent);
                lazy.remove::<RandomReproduction2>(ent);
                lazy.remove::<RandomReproduction3>(ent);
                lazy.remove::<RandomDiseaseMortality>(ent);
                lazy.remove::<RandomAgeMortality>(ent);
            });
        
        self.lg.stop();
    }
}
