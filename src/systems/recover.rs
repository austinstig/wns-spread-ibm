use super::*;

/// # RecoverySystem
/// A system that sets the infection state of bats that
/// survive an outbreak during winter and emerge into
/// summer. The bats are cleared of infection.
pub struct RecoverySystem {
    lg: IntensityLog,
}

impl RecoverySystem {
    pub fn new(outdir: &PathBuf) -> RecoverySystem {
        RecoverySystem {
            lg: IntensityLog::new(outdir, "RecoverySystem.csv")
        }
    }
}


impl<'a> System<'a> for RecoverySystem {

    type SystemData = (Read<'a, Clock>, 
                       Entities<'a>, 
                       ReadStorage<'a, Lifecycle>,
                       WriteStorage<'a, Infected>);

    fn run(&mut self, data: Self::SystemData) {
        self.lg.start();

        // parse out the data
        let (clock, entities, lifecycle, mut infection) = data;

        // only apply clearing the infection on transition to summer
        if clock.is_summer() {

            // iterate over entities and filter out only
            // those that are still alive and update their
            // infection state to show they have recovered.
            (&entities, &lifecycle, &mut infection).par_join()
                .filter(|(_,lf,_)| lf.is_alive())
                .for_each(|(..,state)| state.recover());

        }
        self.lg.stop();
    }
}