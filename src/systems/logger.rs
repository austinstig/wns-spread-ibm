use super::*;

// # OutputRecord
// a record for an output
#[derive(Serialize,Debug)]
struct OutputRecord {
    year: usize,
    roost: usize,
    fips: u32,
    is_infected: Infected,
    species: SpeciesKey,
    count: usize,
}


/// # Logger System
/// A system that logs the progress of the disease each year of the model.
pub struct LoggerSystem {
    year: usize,
    outdir: PathBuf
}

impl LoggerSystem {

    /// construct a new logger system
    pub fn new(output_directory: &PathBuf) -> LoggerSystem {
        // create the output directory 
        // if it does not exist
        if !output_directory.exists() {
            create_dir_all(&output_directory);
        }

        // return a new logger system
        LoggerSystem {
            year: 0,
            outdir: output_directory.clone()
        }
    }

    /// get the next log file
    pub fn logfile(&mut self) -> PathBuf {
        // set the current log file
        let current = self.outdir.join(
            format!("logfile-{0}.csv", self.year)
        );

        // update the year
        self.year += 1;

        // return the log file
        current
    }
}

impl<'a> System<'a> for LoggerSystem {

    type SystemData = (Read<'a, Landscape>,
                       Read<'a, Clock>,
                       Entities<'a>,
                       ReadStorage<'a, RoostId>,
                       ReadStorage<'a, SpeciesKey>
    );

    fn run(&mut self, data: Self::SystemData) {
        
        // parse out the data
        let (landscape, clock, entities, roost_ids, species) = data;

        print!("logging output...");

        // write the output log file
        if let Ok(mut wrtr) = WriterBuilder::new()
                                    .has_headers(true)
                                    .delimiter(b',')
                                    .quote_style(QuoteStyle::Never)
                                    .from_path(self.logfile()) {

            // iterate over the roosts of the landscape
            for roost_idx in landscape.node_indices() {
                if let Some(ref roost) = landscape.node_weight(roost_idx) {

                    // compute count by species for roost
                    let counter: HashMap<SpeciesKey, usize> = (&entities, &roost_ids, &species).par_join()
                        .filter(|(_,rid,_)| rid.value() == roost.id.value())
                        .map(|(_,_,sp)| *sp)
                        .collect::<Vec<SpeciesKey>>()
                        .iter()
                        .fold(HashMap::new(), |mut acc, c| {
                            *acc.entry(*c).or_insert(0) += 1;
                            acc
                        });

                    // print the counter to a csv file
                    counter
                        .iter()
                        .map(|(spname, count)| {
                                OutputRecord {
                                    year: clock.year, 
                                    roost: roost.id.value(),
                                    fips: roost.fips,
                                    is_infected: roost.infected,
                                    species: *spname, 
                                    count: *count
                                }
                        })
                        .for_each(|record| {
                            wrtr.serialize(&record).ok(); // TODO: handle the error somehow
                        });
                }
            }

        }

        println!("complete.");
    }
}