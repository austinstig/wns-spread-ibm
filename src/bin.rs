#![allow(unused_imports)]
#![allow(unused_must_use)]
#![allow(unused_variables)]
#![allow(unused_mut)]

// access the external crates.
extern crate wns;
extern crate specs;
extern crate config;
extern crate structopt;
extern crate rayon;

// import the SPECS prelude.
use specs::prelude::*;
use rayon::prelude::*;
use structopt::StructOpt;
use std::path::PathBuf;
use std::sync::Arc;
use rayon::ThreadPoolBuilder;
use rayon::ThreadPool;

#[derive(StructOpt, Debug)]
#[structopt(name="WNS", about="A whitenose spread simulation model")]
struct Arguments {

    /// input configuration filename
    #[structopt(parse(from_os_str))]
    config: PathBuf,
}

fn main() {

    println!("-w- A WNS Model using ECS and Geoexpression! -w-");

    // load inputs from command line
    let args: Arguments = Arguments::from_args();

    println!("{:?}", args);

    // load the configuration file
    let conf = wns::Configuration::load_config(&args.config)
        .expect("Failed to load configuration!");

    // load the roost site information
    let mut world = wns::init(&conf)
        .expect("Failed to initialize world!");

    // define the systems
    let mut census_system = wns::CensusSystem::new(&conf.output_directory);
    let mut disease_infection_system = wns::DiseaseInfectionSystem::new(&conf.output_directory);
    let mut senesence_system = wns::SenesenceSystem::new(&conf.output_directory);
    let mut age_mortality_system = wns::AgeMortalitySystem::new(&conf.output_directory);
    let mut resource_starvation_mortality_system = wns::ResourceStarvationMortalitySystem::new(&conf.output_directory);
    let mut disease_mortality_system = wns::DiseaseMortalitySystem::new(&conf.output_directory);
    let mut removal_system = wns::RemovalSystem::new(&conf.output_directory);
    let mut movement_system = wns::MovementSystem::new(&conf.output_directory);
    let mut recovery_system = wns::RecoverySystem::new(&conf.output_directory);
    let mut reproduction_system = wns::ReproductionSystem::new(conf.max_pop, &conf.output_directory);
    let mut shed_spore_system = wns::ShedSporeSystem::new(&conf.output_directory);
    let mut grow_pathogen_system = wns::GrowPathogenSystem::new(&conf.output_directory);
    let mut logger_system = wns::LoggerSystem::new(&conf.output_directory);
    let mut rng_system = wns::RNGSystem::new(conf.seed, &conf.output_directory);
    let mut human_movement_system = wns::HumanMovementSystem::new(conf.seed, &conf.output_directory);

    if !conf.dispatch {
        // *** SEQUENTIAL VERSION ***
        let mut clock_system =  wns::ClockSystem;

        // run the model
        for _ in 0..conf.years {

            // **********************************************
            rng_system.run_now(wns::tokens_ref(&world)); // update random numbers
            clock_system.run_now(wns::tokens_ref(&world)); // winter

            // winter human movement
            // human_movement_system.run_now(wns::tokens_ref(&world));

            // infect roost sites
            grow_pathogen_system.run_now(wns::tokens_ref(&world)); // updates to infection after 1 year
            shed_spore_system.run_now(wns::tokens_ref(&world));

            // infect the bats at the roost sites
            disease_infection_system.run_now(wns::tokens_ref(&world));

            // update population counts at roosts
            census_system.run_now(wns::tokens_ref(&world)); 
            logger_system.run_now(wns::tokens_ref(&world));

            // apply mortality (age, disease, population density)
            senesence_system.run_now(wns::tokens_ref(&world));
            age_mortality_system.run_now(wns::tokens_ref(&world));
            disease_mortality_system.run_now(wns::tokens_ref(&world));
            resource_starvation_mortality_system.run_now(wns::tokens_ref(&world));

            removal_system.run_now(wns::tokens_ref(&world)); // remove the dead
            world.maintain(); // maintain the world (remove from ECS)

            // **********************************************
            clock_system.run_now(wns::tokens_ref(&world)); // summer
            recovery_system.run_now(wns::tokens_ref(&world));

            // reproduction of bats
            reproduction_system.run_now(wns::tokens_ref(&world));

            // summer human movement
            human_movement_system.run_now(wns::tokens_ref(&world));

            world.maintain(); // maintain the world (add juveniles to ECS)

            // **********************************************
            clock_system.run_now(wns::tokens_ref(&world)); // swarm
            movement_system.run_now(wns::tokens_ref(&world));
        }
    } else {

        // setup a thread pool
        let thpool = ThreadPoolBuilder::new()
                .num_threads(conf.thread_count)
                .build();

        let pool: Arc<ThreadPool> = match thpool {
            Ok(p) => Arc::new(p),
            Err(e)=> { 
                eprintln!("thread-pool error: {:?}", e);
                ::std::process::exit(2); 
            }
        };


        // *** DISPATCHER VERSION ***
        let clock_system_a =  wns::ClockToWinterSystem;
        let clock_system_b =  wns::ClockToSummerSystem;
        let clock_system_c =  wns::ClockToSwarmSystem;

        let mut winter_dispatch = DispatcherBuilder::new()
            .with_pool(pool.clone())

            .with(clock_system_a, "clock", &[])
            .with(census_system, "census", &["clock"])
            .with(logger_system, "logger", &["census"])

            .with(rng_system, "rng", &["clock"])

            // uncomment for winter human movement
            //.with(human_movement_system, "human", &["clock"])
            //.with(grow_pathogen_system, "grow", &["human"])

            .with(grow_pathogen_system, "grow", &["clock"])
            .with(shed_spore_system, "shed", &["grow"])
            .with(disease_infection_system, "infection", &["shed"])
            .with(senesence_system, "age", &["clock"])
            .with(age_mortality_system, "old_age", &["age","rng"])
            .with(disease_mortality_system, "disease_mortality", &["infection","rng"])
            .with(resource_starvation_mortality_system, "starvation", &["census"])
            .with(removal_system, "removal", &["logger","age","disease_mortality","old_age","starvation"])
            .build();

        let mut summer_dispatch = DispatcherBuilder::new()
            .with_pool(pool.clone())

            .with(clock_system_b, "clock", &[])
            .with(recovery_system, "recover", &["clock"])
            .with(reproduction_system, "reproduction", &["recover"])

            // summer human movement
            .with(human_movement_system, "human", &["clock"])

            .build();

        let mut swarm_dispatch = DispatcherBuilder::new()
            .with_pool(pool.clone())

            .with(clock_system_c, "clock", &[])
            .with(movement_system, "movement", &["clock"])
            .build();

        // run the model
        for _ in 0..conf.years {
            winter_dispatch.dispatch(wns::tokens(&mut world));
            world.maintain();
            summer_dispatch.dispatch(wns::tokens(&mut world));
            world.maintain();
            swarm_dispatch.dispatch(wns::tokens(&mut world));
        }
    }
}