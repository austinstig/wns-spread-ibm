#![allow(unused_imports)]
#![allow(unused_must_use)]
#![allow(unused_variables)]

// import external crate macros
#[macro_use] extern crate derive_is_enum_variant;
#[macro_use] extern crate specs_derive;
#[macro_use] extern crate derive_more;

// type aliases
type KeySpace = u32;
pub type Landscape = Graph<RoostSite, Length, Undirected, KeySpace>;

// load modules
mod functions;
mod io;
mod types;
mod systems;

// re-export the modules
pub use crate::functions::*;
pub use crate::io::*;
pub use crate::types::*;
pub use crate::systems::*;

// preludes
use specs::prelude::*;
use geo::prelude::*;
use petgraph::prelude::*;
use rand::prelude::*;
use rayon::prelude::*;

// load libraries
use colored::*;
use uom::{
    ConversionFactor,
    si::{
        f32::*,
        length::{
            meter,
            kilometer,
        },
    }
};
use log::{
    info,
    trace,
    warn,
    debug,
    error
};
use failure::{
    Fail,
    Error,
};
use petgraph::Graph;
use serde_derive::{
    Deserialize,
    Serialize,
};
use serde::{
    Serializer,
    Serialize,
    Deserializer,
    Deserialize
};
use geo::Point;
use csv::{
    QuoteStyle,
    WriterBuilder,
    Reader
};
use rand::{
    thread_rng,
    Rng,
    distributions::Uniform,
};
use rand_isaac::{
    IsaacRng,
};
use std::{
    collections::HashMap,
    fs::{
        create_dir_all,
        File,
    },
    path::{
        Path,
        PathBuf
    }
};


#[derive(Debug,Fail,From)]
pub enum WNSError {

    #[fail(display="fmt error")]
    FmtError(::core::fmt::Error),

    #[fail(display="csv error")]
    CSVError(::csv::Error),

    #[fail(display="io error")]
    IOError(::std::io::Error),

    #[fail(display="configuration error")]
    ConfigurationError(config::ConfigError),

    #[fail(display="deserialization error")]
    DeserializationError,

    #[fail(display="unknown species")]
    UnknownSpecies
}

#[cfg(test)]
mod tests {

    use crate::io::*;
    use crate::types::*;

    const ROOST_TEST_CONFIG_FILE: &str = "./roosts.csv";
    const SPECIES_TEST_CONFIG_FILE: &str = "./species.csv";

    #[test]
    fn parse_roost_test() {
        if let Ok(records) = load::<RoostSite,_>(ROOST_TEST_CONFIG_FILE) {
            assert_eq!(records[0].id, 1)
        } else { assert!(false) }
    }

    #[test]
    fn distance_test() {
        if let Ok(records) = load::<RoostSite,_>(ROOST_TEST_CONFIG_FILE) {
            let rec1 = &records[0].location;
            let rec2 = &records[1].location;

            // show the distance between two points
            assert_eq!(rec1.distance(&rec2), 111194.93)
        } else {
            assert!(false)
        }
    }

    #[test]
    fn parse_species_test() {
        if let Ok(records) = load::<Species,_>(SPECIES_TEST_CONFIG_FILE) {
            assert_eq!(records[0].name, "taederida")
        } else { assert!(false) }
    }
}